configure_file(mbellei-indicator-datamonitor.py ${CMAKE_CURRENT_BINARY_DIR}/mbellei-indicator-datamonitor.py)

install(FILES ${CMAKE_CURRENT_BINARY_DIR}/mbellei-indicator-datamonitor.py DESTINATION ${DATA_DIR}indicator)
install(FILES "mbellei-indicator-datamonitor.conf" DESTINATION ${DATA_DIR}indicator)
install(FILES "installIndicator.sh" DESTINATION ${DATA_DIR}indicator)
install(FILES "com.mbellei.indicator.datamonitor" DESTINATION ${DATA_DIR}indicator)
install(FILES "uninstallIndicator.sh" DESTINATION ${DATA_DIR}indicator)
