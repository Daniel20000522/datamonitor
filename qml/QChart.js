// QChart.js ---
//
// Author: Julien Wintz
// Created: Thu Feb 13 14:37:26 2014 (+0100)
// Version:
// Last-Updated:
//           By: : M. Bellei
//     Update #:
//

// Change Log:
//
//

var ChartType = {
         BAR: 1,
    DOUGHNUT: 2,
        LINE: 3,
         PIE: 4,
       POLAR: 5,
       RADAR: 6
};

var Chart = function(canvas, context) {

    var chart = this;

// /////////////////////////////////////////////////////////////////
// Bar helper
// /////////////////////////////////////////////////////////////////

    this.Bar = function(data,options) {

        chart.Bar.defaults = {
            scaleOverlay: false,
            scaleOverride: false,
            scaleSteps: null,
            scaleStepWidth: null,
            scaleStartValue: null,
            scaleLineColor: "rgba(0,0,0,.1)",
            scaleLineWidth: 1,
            scaleShowLabels: true,
            scaleLabel: "<%=value%>",
//            scaleFontFamily: "'Arial'",
            scaleFontFamily: "sans-serif",
//            scaleFontSize: 12,
            scaleFontSize: 20,
            scaleFontStyle: "normal",   //M. Bellei: normal/italic/oblique
            scaleFontColor: "#666",
            scaleShowGridLines: true,
            scaleGridLineColor: "rgba(0,0,0,.05)",
            scaleGridLineWidth: 1,
            barShowStroke: true,
            barStrokeWidth: 2,
            barValueSpacing: 5,
            barDatasetSpacing: 1,
            animation: true,
            animationSteps: 60,
            animationEasing: "easeOutQuart",
            onAnimationComplete: null,
            scaleMaxAxisYvalue: 0,
            scaleMaxAxisYStored: 0,
            XLabel: "Days of Month"
        };

        var config = (options) ? mergeChartConfig(chart.Bar.defaults,options) : chart.Bar.defaults;

        return new Bar(data,config,context);
    }

// /////////////////////////////////////////////////////////////////
// Bar implementation
// /////////////////////////////////////////////////////////////////

    var Bar = function(data, config, ctx) {

        var maxSize;
        var scaleHop;
        var calculatedScale;
        var labelHeight;
        var scaleHeight;
        var valueBounds;
        var labelTemplateString;
        var valueHop;
        var widestXLabel;
        var xAxisLength;
        var yAxisPosX;
        var xAxisPosY;
        var barWidth;
        var rotateLabels = 0;
        var finalYscale; //M. Bellei addition
        var clarancesXY; //M. Bellei addition
        var minDistTopYAxis = 30; //M. Bellei addition (15 Dec 2019)
//        var minDistFarXAxis = 130; //M. Bellei addition (15 Dec 2019)
        var minDistFarXAxis = 100; //M. Bellei addition (19 May 2020)
        var smallerFont; //M. Bellei addition (01 July 2020)

        // /////////////////////////////////////////////////////////////////
        // initialisation
        // /////////////////////////////////////////////////////////////////
        this.height = function () {  //Addition by M. Bellei
          return height
        }

        this.width = function () {  //Addition by M. Bellei
          return width
        }

        this.maxYscale = function () {  //Addition by M. Bellei
          return finalYscale
        }

        this.Xclearance = function () {  //Addition by M. Bellei
          return clarancesXY.clearanceX
        }

        this.Y1clearance = function () {  //Addition by M. Bellei
          return clarancesXY.clearanceY1
        }

        this.Y2clearance = function () {  //Addition by M. Bellei
          return clarancesXY.clearanceY2
        }


        this.init = function () {

            calculateDrawingSizes();

            valueBounds = getValueBounds();

            labelTemplateString = (config.scaleShowLabels)? config.scaleLabel : "";

            if (!config.scaleOverride) {
//                calculatedScale = calculateScale(scaleHeight,valueBounds.maxSteps,valueBounds.minSteps,valueBounds.maxValue,valueBounds.minValue,labelTemplateString);
              calculatedScale = calculateScale(scaleHeight,valueBounds.maxSteps,valueBounds.minSteps,valueBounds.maxValue,valueBounds.minValue,labelTemplateString,config.scaleMaxAxisYvalue,config.scaleMaxAxisYStored);  //M. Bellei modification
              finalYscale = calculatedScale.Yvalue;
            } else {
                calculatedScale = {
                    steps: config.scaleSteps,
                    stepValue: config.scaleStepWidth,
                    graphMin: config.scaleStartValue,
                    labels: []
                }
                populateLabels(labelTemplateString, calculatedScale.labels,calculatedScale.steps,config.scaleStartValue,config.scaleStepWidth);
            }

//            scaleHop = Math.floor(scaleHeight/calculatedScale.steps);
            scaleHop = Math.floor((scaleHeight-minDistTopYAxis)/calculatedScale.steps);
            calculateXAxisSize();
        }

        // /////////////////////////////////////////////////////////////////
        // drawing
        // /////////////////////////////////////////////////////////////////

        this.draw = function (progress) {

            clear(ctx);

            if(config.scaleOverlay) {
                drawBars(progress);
                drawScale();
            } else {
//                drawScale();
                clarancesXY = drawScale(); // M. Bellei modification
                drawBars(progress);
            }
        }

        // ///////////////////////////////////////////////////////////////

        function drawBars(animPc) {

            ctx.lineWidth = config.barStrokeWidth;

            for (var i=0; i<data.datasets.length; i++) {
                ctx.fillStyle = data.datasets[i].fillColor;  //<-----M. Bellei note: this is the property to color the bars
                ctx.strokeStyle = data.datasets[i].strokeColor;

                for (var j=0; j<data.datasets[i].data.length; j++) {
                    if (data.datasets[i].data[j]!=0) {   //<-----M. Bellei note: added in case of value equal to 0, in order not to draw line on x axis
                      var barOffset = yAxisPosX + config.barValueSpacing + valueHop*j + barWidth*i + config.barDatasetSpacing*i + config.barStrokeWidth*i;

                      ctx.beginPath();
                      ctx.moveTo(barOffset, xAxisPosY);
                      ctx.lineTo(barOffset, xAxisPosY - animPc*calculateOffset(data.datasets[i].data[j],calculatedScale,scaleHop)+(config.barStrokeWidth/2));
                      ctx.lineTo(barOffset + barWidth, xAxisPosY - animPc*calculateOffset(data.datasets[i].data[j],calculatedScale,scaleHop)+(config.barStrokeWidth/2));
                      ctx.lineTo(barOffset + barWidth, xAxisPosY);
                      if(config.barShowStroke) {
                          ctx.stroke();
                      }
                      ctx.closePath();
                      ctx.fill();
                    }
                }
            }
        }

        function drawScale() {

            ctx.lineWidth = config.scaleLineWidth;
            ctx.strokeStyle = config.scaleLineColor;
            ctx.beginPath();
            ctx.moveTo(width-widestXLabel/2+5,xAxisPosY);
//            ctx.lineTo(width-(widestXLabel/2)-xAxisLength-5,xAxisPosY);
            ctx.lineTo(width-(widestXLabel/2)-xAxisLength,xAxisPosY); //M. Bellei: X and Y axises crossing point better looking
            ctx.stroke();

//            if (rotateLabels > 0) {
//                ctx.save();
//                ctx.textAlign = "right";
//            } else{
                ctx.textAlign = "center";
//            }

            ctx.fillStyle = config.scaleFontColor;

            var textLength = ctx.measureText("28").width;   //M. Bellei addition (26 June 2020)
            var k=1;
            while  (textLength>=valueHop-4 & k<7) {               //M. Bellei addition (26 June 2020)
              smallerFont = config.scaleFontSize - k;         //M. Bellei addition (26 June 2020)
              textLength = ctx.measureText("28").width; //M. Bellei addition (26 June 2020)
              k=k+1; //M. Bellei addition (26 June 2020)
            } //M. Bellei addition (26 June 2020)

            ctx.font = config.scaleFontStyle + " " + smallerFont+"px " + config.scaleFontFamily; //M. Bellei addition (26 June 2020)

            for (var i=0; i<data.labels.length; i++) {    //M. Bellei: Xaxis scale labels drawing
                ctx.save();
//                if (rotateLabels > 0) {
//                    ctx.translate(yAxisPosX + i*valueHop,xAxisPosY + config.scaleFontSize);
//                    ctx.rotate(-(rotateLabels * (Math.PI/180)));
//                    ctx.fillText(data.labels[i], 0,0);
//                    ctx.restore();
//                } else {
                    ctx.fillText(data.labels[i], yAxisPosX + i*valueHop + valueHop/2,xAxisPosY + config.scaleFontSize+3);
//                }

                ctx.beginPath();
                ctx.moveTo(yAxisPosX + (i+1) * valueHop, xAxisPosY+3);
                ctx.lineWidth = config.scaleGridLineWidth;
                ctx.strokeStyle = config.scaleGridLineColor;
                ctx.lineTo(yAxisPosX + (i+1) * valueHop, 5);
                ctx.stroke();
            }
            ctx.font = config.scaleFontStyle + " " + config.scaleFontSize+"px " + config.scaleFontFamily; //M. Bellei addition (26 June 2020)
            ctx.fillText(config.XLabel, width-minDistFarXAxis,xAxisPosY + 2.5*config.scaleFontSize);  //M. Bellei: added label for the days shown on Xaxis (15 Dec 2019)

            ctx.lineWidth = config.scaleLineWidth;
            ctx.strokeStyle = config.scaleLineColor;
            ctx.beginPath();
//            ctx.moveTo(yAxisPosX,xAxisPosY+5);
            ctx.moveTo(yAxisPosX,xAxisPosY);   //M. Bellei: X and Y axises crossing point better looking
            ctx.lineTo(yAxisPosX,5);
            ctx.stroke();
            ctx.textAlign = "right";
            ctx.textBaseline = "middle";

//            ctx.font = config.scaleFontStyle + " " + config.scaleFontSize+"px " + config.scaleFontFamily; //M. Bellei addition (26 June 2020)

            for (var j=0; j<calculatedScale.steps; j++) {      //M. Bellei: Yaxis scale labels drawing
                ctx.beginPath();
                ctx.moveTo(yAxisPosX-3,xAxisPosY - ((j+1) * scaleHop));
                if (config.scaleShowGridLines) {
                    ctx.lineWidth = config.scaleGridLineWidth;
                    ctx.strokeStyle = config.scaleGridLineColor;
                    ctx.lineTo(yAxisPosX + xAxisLength + 5,xAxisPosY - ((j+1) * scaleHop));
                } else {
                    ctx.lineTo(yAxisPosX-0.5,xAxisPosY - ((j+1) * scaleHop));
                }
                ctx.stroke();
                if (config.scaleShowLabels) {
                    ctx.fillText(calculatedScale.labels[j],yAxisPosX-8,xAxisPosY - ((j+1) * scaleHop));
                }
            }
            ctx.fillText("MBytes",yAxisPosX-8, 20);  //M. Bellei: added label for the Mbytes shown on Yaxis (15 Dec 2019)
            return {
                clearanceY1: height-xAxisPosY,  //M. Bellei addition
                clearanceY2: xAxisPosY - j*scaleHop,  //M. Bellei addition
//                clearanceY: minDistTopYAxis,  //M. Bellei addition
                clearanceX: yAxisPosX
            };
        }

        function calculateXAxisSize() {

            var longestText = 1;

            if (config.scaleShowLabels) {

                ctx.font = config.scaleFontStyle + " " + config.scaleFontSize+"px " + config.scaleFontFamily;  //M. Bellei: it is fundamental that the scaleFontFamily is recognized by the ctx.font property, otherwise it remains stuck to the default "10px sans-serif"

                for (var i=0; i<calculatedScale.labels.length; i++) {
                    var measuredText = ctx.measureText(calculatedScale.labels[i]).width;
                    longestText = (measuredText > longestText)? measuredText : longestText;
                    if (i==calculatedScale.labels.length-1) { //M.Bellei addition (18 May 2020)
                      var measuredText = ctx.measureText("MBytes").width; //M.Bellei addition (18 May 2020)
                      longestText = measuredText //M.Bellei addition (18 May 2020)
                    }
                }

                if (calculatedScale.labels.length==0) { //M.Bellei addition (20 Dec 2019)
                  var measuredText = ctx.measureText("MBytes").width; //M.Bellei addition (20 Dec 2019)
                  longestText = measuredText //M.Bellei addition (20 Dec 2019)
                } //M.Bellei addition (20 Dec 2019)

            }

            xAxisLength = width - longestText - widestXLabel;
            valueHop = xAxisLength/(data.labels.length); //M.Bellei addition (18 May 2020)

            barWidth = (valueHop - config.scaleGridLineWidth - (config.barValueSpacing) - (config.barDatasetSpacing*data.datasets.length-1) - ((config.barStrokeWidth/2)*data.datasets.length-1))/data.datasets.length;

            yAxisPosX = width-widestXLabel/2-xAxisLength;
            xAxisPosY = scaleHeight + config.scaleFontSize/2;
        }

        function calculateDrawingSizes() {

            maxSize = height;
//            ctx.font = config.scaleFontStyle + " " + config.scaleFontSize+"px " + config.scaleFontFamily;
            var textLength = ctx.measureText("28").width;   //M. Bellei addition (26 June 2020)
            widestXLabel = textLength; //M. Bellei addition (26 June 2020)
            var k=1;
            while  (textLength>=valueHop-4 & k<7) {               //M. Bellei addition (26 June 2020)
              smallerFont = config.scaleFontSize - k;         //M. Bellei addition (26 June 2020)
//              ctx.font = config.scaleFontStyle + " " + smallerFont+"px " + config.scaleFontFamily; //M. Bellei addition (26 June 2020)
              textLength = ctx.measureText("28").width; //M. Bellei addition (26 June 2020)
              k=k+1; //M. Bellei addition (26 June 2020)
            } //M. Bellei addition (26 June 2020)
//            widestXLabel = 1;

            //fulvio custom fix: added if check due to console errors: TypeError: Cannot read property 'labels' of undefined
            if(data !==undefined)
            {

//            for (var i=0; i<=data.labels.length; i++) {                        //M. Bellei deletion (26 June 2020)
//                if (i<data.labels.length) { //M.Bellei addition (15 Dec 2019)
//                  var textLength = ctx.measureText(data.labels[i]).width;
//                  widestXLabel = (textLength > widestXLabel)? textLength : widestXLabel;
//                }
//            }
//                var textLength = ctx.measureText("28").width; //M. Bellei addition (26 June 2020)
//                widestXLabel = textLength; //M. Bellei addition (26 June 2020)
                maxSize -= config.scaleFontSize;

            } //end fix

            labelHeight = config.scaleFontSize;

//            maxSize -= 2*labelHeight; //M. Bellei modification
            maxSize -= smallerFont+config.scaleFontSize; //M. Bellei modification

            scaleHeight = maxSize;
        }

        function getValueBounds() {

            var upperValue = Number.MIN_VALUE;
            var lowerValue = Number.MAX_VALUE;

            //fulvio custom fix: added if check due to console errors: TypeError: Cannot read property 'datasets' of undefined
            if(data !==undefined)
            {

            for (var i=0; i<data.datasets.length; i++) {
                for (var j=0; j<data.datasets[i].data.length; j++) {
                    if ( data.datasets[i].data[j] > upperValue) { upperValue = data.datasets[i].data[j] };
                    if ( data.datasets[i].data[j] < lowerValue) { lowerValue = data.datasets[i].data[j] };
                }
            };

            }//end fix

            var maxSteps = Math.floor((scaleHeight / (labelHeight*0.66)));
            var minSteps = Math.floor((scaleHeight / labelHeight*0.5));

            return {
                maxValue: upperValue,
                minValue: lowerValue,
                maxSteps: maxSteps,
                minSteps: minSteps
            };
        }
    }

// /////////////////////////////////////////////////////////////////
// Helper functions
// /////////////////////////////////////////////////////////////////

    var clear = function(c) {
        c.clearRect(0, 0, width, height);
    };


    function calculateOffset(val,calculatedScale,scaleHop) {

        var outerValue = calculatedScale.steps * calculatedScale.stepValue;
        var adjustedValue = val - calculatedScale.graphMin;
        var scalingFactor = CapValue(adjustedValue/outerValue,1,0);

        return (scaleHop*calculatedScale.steps) * scalingFactor;
    }

    function calculateScale(drawingHeight,maxSteps,minSteps,maxValue,minValue,labelTemplateString,maxYvalue,maxYStored) {

        var graphMin,graphMax,graphRange,stepValue,numberOfSteps,valueRange,rangeOrderOfMagnitude,decimalNum;

        valueRange = maxValue - minValue;
        rangeOrderOfMagnitude = calculateOrderOfMagnitude(valueRange);
        graphMin = Math.floor(minValue / (1 * Math.pow(10, rangeOrderOfMagnitude))) * Math.pow(10, rangeOrderOfMagnitude);
        graphMax = Math.ceil(maxValue / (1 * Math.pow(10, rangeOrderOfMagnitude))) * Math.pow(10, rangeOrderOfMagnitude);
        if (!isNumber(graphMax)) {
           maxYvalue=graphMax;
        } else {
          if (!isNumber(maxYvalue)) {
              if (graphMax<maxYStored) {
                  if (maxYStored<5) {
                      maxYStored=5
                  }
                  graphMax=maxYStored;
              } else {
                  if (graphMax<5) {
                      graphMax=5
                  }
              }
              maxYvalue=graphMax;
          } else {
              if (graphMax<maxYvalue) {
                    if (maxYvalue<5) {
                        maxYvalue=5
                    }
                    graphMax=maxYvalue;
              } else {
                  if (graphMax<5) {
                        graphMax=5
                  }
                  maxYvalue=graphMax;
              }
          }
        }
        graphRange = graphMax - graphMin;

        numberOfSteps = Math.floor(maxSteps/2); //M.Bellei addition (15 Jan 2020): set a convenient number of constant steps
        stepValue = graphRange / numberOfSteps; //M.Bellei addition (14 Dec 2019): added to make number at top of axis y constant after rotation of screen from vertical to horizontal
        if (!isNumber(graphRange)) {
          numberOfSteps = 0
        }

        var labels = [];

        populateLabels(labelTemplateString, labels, numberOfSteps, graphMin, stepValue);

        return {
            steps: numberOfSteps,
            stepValue: stepValue,
            graphMin: graphMin,
            labels: labels,
            Yvalue: maxYvalue  //M. Bellei addition
        }

        function calculateOrderOfMagnitude(val) {
            return Math.floor(Math.log(val) / Math.LN10);
        }
    }

    function populateLabels(labelTemplateString, labels, numberOfSteps, graphMin, stepValue) {
        if (labelTemplateString) {
            for (var i = 1; i < numberOfSteps + 1; i++) {
                labels.push(tmpl(labelTemplateString, {value: (graphMin + (stepValue * i)).toFixed(getDecimalPlaces(stepValue))}));
            }
        }
    }

    function Max(array) {
        return Math.max.apply(Math, array);
    };

    function Min(array) {
        return Math.min.apply(Math, array);
    };

    function Default(userDeclared,valueIfFalse) {
        if(!userDeclared) {
            return valueIfFalse;
        } else {
            return userDeclared;
        }
    };

    function isNumber(n) {
        return !isNaN(parseFloat(n)) && isFinite(n);
    }

    function CapValue(valueToCap, maxValue, minValue) {
        if(isNumber(maxValue)) {
            if( valueToCap > maxValue ) {
                return maxValue;
            }
        }
        if(isNumber(minValue)) {
            if ( valueToCap < minValue ) {
                return minValue;
            }
        }
        return valueToCap;
    }

    function getDecimalPlaces (num) {
        var numberOfDecimalPlaces;
        if (num%1!=0) {
//            return num.toString().split(".")[1].length
            return 1  // M. Bellei modification: only one decimal digit is enough for Mbytes data
        } else {
            return 0;
        }
    }

    function mergeChartConfig(defaults,userDefined) {
        var returnObj = {};
        for (var attrname in defaults) { returnObj[attrname] = defaults[attrname]; }
        for (var attrname in userDefined) { returnObj[attrname] = userDefined[attrname]; }
        return returnObj;
    }

    var cache = {};

    function tmpl(str, data) {
        var fn = !/\W/.test(str) ?
            cache[str] = cache[str] ||
            tmpl(document.getElementById(str).innerHTML) :

        new Function("obj",
                     "var p=[],print=function() {p.push.apply(p,arguments);};" +
                     "with(obj) {p.push('" +
                     str
                     .replace(/[\r\t\n]/g, " ")
                     .split("<%").join("\t")
                     .replace(/((^|%>)[^\t]*)'/g, "$1\r")
                     .replace(/\t=(.*?)%>/g, "',$1,'")
                     .split("\t").join("');")
                     .split("%>").join("p.push('")
                     .split("\r").join("\\'")
                     + "');}return p.join('');");

        return data ? fn( data ) : fn;
    };
}

// /////////////////////////////////////////////////////////////////
// Credits
// /////////////////////////////////////////////////////////////////

/*!
 * Chart.js
 * http://chartjs.org/
 *
 * Copyright 2013 Nick Downie
 * Released under the MIT license
 * https://github.com/nnnick/Chart.js/blob/master/LICENSE.md
 */

// Copyright (c) 2013 Nick Downie

// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy,
// modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
// BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
// ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
