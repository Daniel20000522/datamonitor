## dataMonitor

This is the first app for Ubuntu Touch OS (UBports community edition) which brings the data usage monitoring capability on this platform, mainly based on the QNetworkConfigurationManager API.

The present app is still in test stage and its current version is 0.1.9.4.

!!!WARNING: the dataMonitor app works UNCONFINED, due to the need of getting the data statistics from the Network and to the employment of a daemon service, which is designed to execute the Actiondaemon binary every a determined time step, in order to have the received Mbytes database always up to date.

The received Mbytes database is automatically generated once installed the daemon files and run the Actiondaemon binary for the first time (after phone re-boot). The daemon runs in background, so it could be possible that it could eventually have an impact on battery discharging rate (I will run some tests later to prove the actual impact). If you want to stay on the safe side and you want to avoid any remote battery drainage in certain circumstances, you can easily delete the daemon files easily by opening the Settings page and tapping on the 'remove daemon' button; bear in mind that every time you open the dataMonitor app again, the daemon files will be automatically restored back.

# Useful directories:

- app main directory is /opt/click.ubuntu.com/datamonitor.matteobellei;

- mattdaemon.conf, mattdaemon-service.conf and mbellei-indicator-datamonitor.conf are stored in .config/upstart;
  IMPORTANT REMARK: due to the fact that the present app is unconfined and in order to work properly it needs storing the aforementioned daemon files in the .config/upstart location, outside the "conventional" boundaries of the app, every time the user wishes to uninstall the app for any reason, he/she shall be aware of the following procedure: prior to uninstalling the app, it is recommended to first remove the daemon files and Indicator files from Settings; in case the app was removed without prior deleting the files from within the app itself, it's still possible to manually remove the daemon and indicator files, following this procedure: for removal of the daemon files open the Terminal app and type `cd .config/upstart`, then type `rm -r mattdaemon*` and `rm -r mbellei-indicator*`, whilst for removal of the optional indicator file type `cd .local/share/unity/indicators/` and then `rm -r com.mbellei*`.

- mattdaemon.log, mattdaemon-service.log are stored in .cache/upstart;

- received Mbytes database is stored in /.local/share/datamonitor.matteobellei/Databases;

- additional service .conf files are stored in /.config/datamonitor.matteobellei;

- indicator file com.mbellei.indicator.datamonitor is stored at .local/share/unity/indicators/.

# Build instructions

To build the app from sources you need to open a Terminal shell on your computer and type from there:

- Go to the folder where the app files are stored by typing `cd /yourdataMonitorAppFolder`;
- type `clickable build-libs -a yourArch`, where yourArch is a place holder for the architecture you want to build to (i.e. armhf, arm64 or amd64);
- type `clickable -a yourArch`, where yourArch is a place holder for the architecture you want to build to (i.e. armhf, arm64 or amd64) to build on your device (phone, Raspberry Pi or tablet), being sure your device is connected to your PC.

# Change logs:

# -Version 0.1.9.4-

1) Previous fix for issue #10 "Horizontal axis shows days labels overlapped on some devices (e.g. BQ Aquaris E5 HD)" generated a regression which the present release tries to resolve.

2) Fix for issue #11 "Sometimes received data for one connection, either Wi-Fi or SIM, are stored for both, when both are enabled on the device". In addition, networkdaemon.cpp was also modified for the user case with 2 SIM cards enabled at the same time.

# -Version 0.1.9.3-

1) Additional fix for issue #5 "Inconsistent values (even negative)", due to incoherent behavior at the changing of month; Actiondaemon.cpp file modified.

2) Fix for issue #9 "remove daemon does not give user feedback".

3) Fix for issue #10 "Horizontal axis shows days labels overlapped on some devices (e.g. BQ Aquaris E5 HD)".

4) Minor labels tinkering in the "Settings" section, to displaying more clearly text and messages.

# -Version 0.1.9.2-

1) Fix to issue #5 "Inconsistent values (even negative)"; Actiondaemon.cpp file modified.

# -Version 0.1.9-

1) A brand new app restyling has been introduced with the present version. All the credits go to Michele (@mymike00 on Telegram) who made that possible! Thank you again Michele, this app is becoming more and more the flagship contribution of the Italian Community for the UBports - Ubuntu Touch project.

2) New gesture added for the graph: starting since now you can long swipe to the right/left on the graph to have a look at the data usage archived for the earlier/later months.

3) Added new Gestures info page and Credits page to the Information page of the app.

4) For the graph, modified X axis scale to accommodate better the days labels and enlarged bars graphical horizontal edge (smaller gap between two adjacent bars) within each time step.

5) Minor bug fixing.

# -Version 0.1.8-

1) Indicator facility added for a quicker glance at data usage. It's optional and the user can decide whether to install it or not, from `Settings`. The implementation is based on the indicator-weather by B. Douglass and the recently released indicator-upower by Ernest: thanks to both the developers.

2) Merged MR made by Jonatan Ziegler, who modified the mattdaemon.conf file and the relevant CMakeLists.txt to be architecture agnostic. To have the new file in place, you need to head to `Settings`, remove the daemon files by means of the dedicated button, close the app and re-open soon after to have automatically installed the new daemon files.

3) Fixed a bug preventing to build for the arm64 architecture: clickable complained for too many zeroes of a constant number defined in the networkdaemon.cpp file only for being as big as possible but exceeding the long type capability. Reducing the number of zeroes did the trick.


# -Version 0.1.7-

1) Fixed bug which prevented the app to work for the SIM connection (feedback needed on this).

2) Daemon .conf files modified t be more useful and simple. To have the new files in place, you need to head to `Settings`, remove the daemon files by means of the dedicated button, close the app and re-open soon after to have automatically installed the new daemon files.

3) Added new settings in the `Settings` page:
    1. Switch to enable colors by theme global selection or to be custom set; user can now set a custom color + opacity for the background, lines, font, buttons, graph grid and bars;
    2. Switch + button to enable the global theme selection by daylight (between Ambiance and SuruDark only), setting a threshold time;
    3. Slider to custom set the daemon time step for its idle activity, to better control the battery energy drain.

4) Minor bug fixes.

5) EDIT: generated the first .click file for arm64 architecture. It carries however a bug related to the folder where to find the Actiondaemon bynary, which depends on the building architecture selected. This bug was discovered by Jonatan Zeidler who fixed that issue by submitting an MR. This will be merged in the next version of the app. Thank you Jonatan for that, all the credits go to you!


# -Version 0.1.6-

1) A new icon has been introduced with the present release (I'm not a great artist so this could be a temporary solution in case somebody else would come up with a better idea);
2) Added notifications in case of data thresholds exceeding. The code employs the C++ function 'popen' which apparently is the only one I discovered to work seamlessly without killing the daemon ('system' and 'QProcess' were the other two methods tried before);
3) Added summary at the Main.qml page for SIM and Wi-fi data;
4) Added the "info" section on the up right corner, close to "settings";
5) Merged early translation to spanish thanks to @Krakakanov (new additional strings to be translated have been added since then);
6) Added translation to italian thanks to...me;
7) Added gestures to interact with the graph:
    1. Scroll down/up with two fingers to enlarge/narrow the 'MBytes' data range;
    2. Press and hold with one finger to create a new data threshold object;
8) Added 'BottomEdge' section to store the list of the created data thresholds. In this section you can:
    1. Drag the data threshold row towards right, to delete the data threshold;
    2. Drag the data threshold row towards left, to mute the notification for the concerned threshold;
9) Added labels for 'Mbytes' and 'Days of Month';
10) Fixed 'Mbytes' data steps to be consistent between the Vertical and Horizontal view and to fill all the space at its disposal;
11) Fixed bug occurring when the date passed from one day to the day after, not calculating the received data in a correct way;
12) Added message on the middle of the page to inform about data unavailability when opening a graph with no data yet;
13) Added message on the middle of the 'BottomEdge' page to inform about thresholds unavailability when no data thresholds yet.
